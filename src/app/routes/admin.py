from __future__ import annotations
from typing import List, Union

def get_dynamic_table(orders: List[List[Union[str, int]]]) -> List[List[Union[str, int]]]:
    """
    orders besteht aus einer Liste aus Listen. Eine Liste besteht jeweils aus einer order_item_id (int), einer order_id (int) und
    dem product_name (str).

    :param orders: list[list[str | int]] Rohe Daten, aus welchen eine Statistik erstellt wird
    :return: das 2d Array mit der Statistik mit den product_name als Spaltennamen und der order_id als Reihennamen
    """

    if not orders:
        return orders

    all_product_names = []
    all_orders = {}

    for order_item_id, order_id, product_name in orders:
        # Collect unique product names for column headers
        if product_name not in all_product_names:
            all_product_names.append(product_name)

        # Counts the number of each product_name in the order per order_id
        if order_id in all_orders:
            if product_name in all_orders[order_id]:
                all_orders[order_id][product_name] += 1
            else:
                all_orders[order_id][product_name] = 1
        else:
            all_orders[order_id] = {product_name: 1}

    # Sort column headers in lexicographical order (by chars in unicode value range) ascending from low to high
    all_product_names = sorted(all_product_names)
    all_column_headers = ["Order ID"] + all_product_names + ["Total"]

    dynamic_table = [all_column_headers]

    # Fill 2d array with values, where column headers are product name and row headers are order id
    for row_order_id in sorted(all_orders, key=int):  # Sorts tables ascending by order_id from low int to high int
        row = [row_order_id]
        total_per_order = 0
        for product_name in all_product_names:
            count = all_orders[row_order_id].get(product_name, 0)
            row.append(count)
            total_per_order += count
        row.append(total_per_order)
        dynamic_table.append(row)

    # Creates total row containing the total for each column
    total_row = ["Total"]
    for col_index in range(1, len(all_column_headers)):
        total = sum(row[col_index] for row in dynamic_table[1:])
        total_row.append(total)

    dynamic_table.append(total_row)

    return dynamic_table

def calculate_row_totals(dynamic_table: List[List[Union[str, int]]]) -> List[List[Union[str, int]]]:
    return dynamic_table

def calculate_column_totals(dynamic_table: List[List[Union[str, int]]]) -> List[List[Union[str, int]]]:
    pass
