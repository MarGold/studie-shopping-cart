from __future__ import annotations
from typing import List, Dict, Union

def get_prices_from_cart(products: List[Dict[str, Union[int, str]]]) -> List[int]:
    prices: List[int] = []

    for product in products:
        prices.append(product["price"])

    return prices

def calculate_product_sum_with_discount_util(products: List[Dict[str, Union[int, str]]]) -> int:
    prices = get_prices_from_cart(products)
    discounted_prices = []

    for i in range(len(prices)):
        current_price = prices[i]
        discount = 0

        for j in range(i + 1, len(prices)):
            if prices[j] <= current_price:
                discount = prices[j]
                break

        discounted_prices.append(current_price - discount)

    total_price = sum(discounted_prices)
    return total_price

# Beispielverwendung
products = [
    {"name": "Produkt 1", "price": 10},
    {"name": "Produkt 2", "price": 9},
    {"name": "Produkt 3", "price": 10},
    {"name": "Produkt 4", "price": 8},
    {"name": "Produkt 5", "price": 8}
]

total_price = calculate_product_sum_with_discount_util(products)
print(f"Die Gesamtsumme des Warenkorbs mit Rabatt beträgt: {total_price}€")
